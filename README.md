Run it using ./slides or cmd slides.exe on windows from the directory containing the binaries.

View slides at localhost:8080.

Test slides execute curl command:
```bash
curl -i  -XPUT 'http://localhost:8080/update' -d '["slide one", "slide two", "slide red", "slide blue", "<section>\n<h2>EmbeddedMediaTest</h2>\n</section>\n\n<section>\n<iframe data-autoplay width=\"420\" height=\"345\" src=\"http://www.youtube.com/embed/l3RQZ4mcr1c\"></iframe>\n</section>\n\n<section>\n<h2>Empty Slide</h2>\n</section>\n"]'
```
