package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
)

var slides []template.HTML
var tmpl *template.Template

func mainHandler(w http.ResponseWriter, r *http.Request) {
	tmpl.ExecuteTemplate(w, "main", slides)
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		e := fmt.Errorf("failed to parse update body: %v", err)
		http.Error(w, e.Error(), 400)
		log.Println(e)
		return
	}
	if err := json.Unmarshal(body, &slides); err != nil {
		http.Error(w, err.Error(), 400)
		log.Println("failed to update slides:", err)
	} else {
		log.Println("updated slides from", r.RemoteAddr)
	}
}

func main() {
	tmpl = template.Must(template.ParseFiles("index.html"))

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/", mainHandler)
	http.HandleFunc("/update", saveHandler)

	log.Println("Listening...")
	http.ListenAndServe(":8080", nil)
}
